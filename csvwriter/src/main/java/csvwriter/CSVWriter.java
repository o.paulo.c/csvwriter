package csvwriter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class CSVWriter {
    public void run(){
       
        System.out.println("Reading original dataset...");
       
        ArrayList<ArrayList<Double>> dimensions = new ArrayList<>();
        SecureRandom random = new SecureRandom();

        try(BufferedReader bufferedReader = new BufferedReader(new FileReader("IRIS.csv"))) {
            String row;
            int lines = 0;
            final int columnIgnored = 1;
            int dimensionSize = 0;
   		 	while((row = bufferedReader.readLine()) != null) {
                String[] data = row.split(",");
                if(lines++ == 0){
                    dimensionSize = data.length - columnIgnored;
                    for (int i=0; i < dimensionSize; i++) {
                        dimensions.add(new ArrayList<Double>());
                    }
                    continue;
                }
                for (int i=0; i < dimensionSize; i++) {
                    dimensions.get(i).add(Double.parseDouble(data[i]));
                }
   		 	}
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (ArrayList<Double> column : dimensions) {
            double max = Double.MIN_VALUE;
            double min = Double.MAX_VALUE;
            for (double value : column) {
                if(value > max) max = value;
                else if(value < min) min = value;
            }
            column.clear();
            column.add(min);
            column.add(max);
        }
       

        System.out.println("Enter the number of lines: ");
        Scanner input = new Scanner(System.in);
        int lines = input.nextInt();
        input.close();
        
        
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);
        df.applyPattern("0.00");

        try (BufferedWriter writer = new BufferedWriter(new FileWriter("test.csv"))) {
            StringBuilder dataText = new StringBuilder();
            for(int i = 1; i <= lines; i++){
                for (ArrayList<Double> col : dimensions) {
                    dataText.append(df.format(random.nextDouble(col.get(0), col.get(1))));
                    dataText.append(",");
                }
                dataText.setCharAt(dataText.length()-1,'\n');
            }
            writer.write(dataText.toString());
            System.out.println("CSV generated!");
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }    
}
